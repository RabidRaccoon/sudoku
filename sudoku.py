import random
import copy

class Sudoku:

	def __init__(self):
		'''Initializes the Sudoku'''

	def valid_guess(self, puzzle, guess, row, col):
		'''Takes an unsolved sudoku puzzle, a guess and a location in the puzzle
		and checks to see if the guess is valid in that location'''

		# Checks the row to see if its a valid guess
		for x in range(9):
			if puzzle[x][col] == guess:
				return False

		# Checks column to see if its a valid guess
		for y in range(9):
			if puzzle[row][y] == guess:
				return False
		
		# Checks the box to see if its a valid guess
		xbox = (row // 3) * 3
		ybox = (col // 3) * 3
		for x in range(xbox,xbox+3):
			for y in range(ybox, ybox + 3):
				if puzzle[x][y] == guess:
					return False

		return True


	def find_empty(self, puzzle):
		'''Takes a sudoku puzzle and looks for the next empty location represented by
		a 0 and returns the location'''

		for x in range(9):
			for y in range(9):
				if puzzle[x][y] == 0:
					return x, y
		return False


	def sudoku_solver(self, puzzle):
		"""Solves a sudoku puzzle using recursive backtracking"""
		
		# Looks for the next empty location and sets them to the row and col
		empty = self.find_empty(puzzle)
		if not empty:
			return True
		else:
			row, col = empty

		# Generates a random list of guesses between 1-9
		rannum = []
		numbers = [1,2,3,4,5,6,7,8,9]
		for y in range(9):
			num = random.randint(0,(len(numbers)-1))
			rannum.append(numbers[num])
			numbers.remove(numbers[num])
		
		# Iterates through the list of guesses and checks to see if its a valid guess
		# If the guess is valid it replaces the empty location
		for i in rannum:
			if self.valid_guess(puzzle, i, row, col):
				puzzle[row][col] = i

				# Uses recursion to fill out the sudoku puzzle
				if self.sudoku_solver(puzzle):
					return True

				# If no possible solution sets the guess to zero and then iterates through
				# the next loop
				puzzle[row][col] = 0

		return False

	def printboard(self, puzzle):
		'''Prints out a formatted version of the matrix to looks like a sudoku puzzle'''
		output = ""

		for x in range(9):
			for y in range(9):
				output = output + str(puzzle[x][y]) + " "
				if (y + 1) // 3 <= 2 and (y + 1) % 3 == 0:
					output = output + "| "
				elif y == 8:
					output = output + "\n"
			if (x + 1) // 3 <= 2 and (x + 1) % 3 == 0:
				output = output + "------+-------+------\n" 
		print(output)

	def sudoku_builder(self):
		'''Fills an empty sudoku puzzle and then removes elements to a sudoku puzzle'''

		difficulty = 3
		count = 0
		saved = False
		num = 0
		puzzle = [[0,0,0,0,0,0,0,0,0],
				  [0,0,0,0,0,0,0,0,0],
				  [0,0,0,0,0,0,0,0,0],
				  [0,0,0,0,0,0,0,0,0],
				  [0,0,0,0,0,0,0,0,0],
				  [0,0,0,0,0,0,0,0,0],
				  [0,0,0,0,0,0,0,0,0],
				  [0,0,0,0,0,0,0,0,0],
				  [0,0,0,0,0,0,0,0,0]]

		# Generates a filled sudoku
		self.sudoku_solver(puzzle)
		solution = copy.deepcopy(puzzle)

		
		# Depending on specified difficult determines a random number of elements to remove from
		# the soduku puzzle
		guesses = []
		if difficulty == 3:
			diffcount = random.randint(50, 53)
		if difficulty == 2:
			diffcount = random.randint(46, 49)
		if difficulty == 1:
			diffcount = random.randint(40, 45)

		# Creates a list of guesses for a 9x9 grid
		for i in range(0,9):
			for j in range(0,9):
				guesses.append((i,j))


		#Loops through all possible guesses removing number if the result puzzle has a unique solution
		while count < diffcount:
			guess = random.randint(0,len(guesses)-1)
			x = guesses[guess][0]
			y = guesses[guess][1]
			if puzzle[x][y] != 0:
				store_before_change = copy.deepcopy(puzzle)
				puzzle[x][y] = 0
				store_after_change = copy.deepcopy(puzzle)

				# Removes the guess from the list to help speed up the program
				guesses.remove(guesses[guess])
				
				# Solves the puzzle 6 times to see if it gets the same solution. If its true then it keeps
				# the change, if not it reverts back 
				if self.test_puzzle(puzzle, solution, 6) == True:
					puzzle = copy.deepcopy(store_after_change)
					saved = True
				else:
					puzzle = copy.deepcopy(store_before_change)

				# If a number is removed, increments a count
				if saved == True:
					count += 1
					saved = False
		
		self.printboard(puzzle)

		# Double checks that the puzzle has a unique solution by solving it 10 times
		if self.test_puzzle(puzzle, solution, 10):
			print("Unique Solution\n")
		else:
			print("Solution is not Unique\n")

		self.sudoku_solver(puzzle)
		self.printboard(puzzle)

		
	def test_puzzle(self, puzzle, solution, num):
		'''Solves the puzzle num times and compares it to solution. If this test fails it means
		the solution is not unique'''
		store = copy.deepcopy(puzzle)
		for i in range(num):
			self.sudoku_solver(puzzle)
			if puzzle != solution:
				return False					
			puzzle = copy.deepcopy(store)
		return True



